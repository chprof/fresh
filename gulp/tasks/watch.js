module.exports = function() {
	$.gulp.task("watch", function() {
		return new Promise((res, rej) => {
			$.gp.watch(["./app/**/*.html", "./app/*.php",], $.gulp.series("html"));
			$.gp.watch("./app/js/**/*.js", $.gulp.series("scripts"));
			$.gp.watch("./app/scss/**/*.scss", $.gulp.parallel("styles", "criticalCss", "vendor"));
			// $.gp.watch("./app/scss/critical/**/*.scss", $.gulp.series("criticalCss", "styles"));
			// $.gp.watch("./app/scss/vendor/**/*.scss", $.gulp.series("vendor"));
			res();
		});
	});
};